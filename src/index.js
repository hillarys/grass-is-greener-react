import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import App from './containers/App';

import './index.css';

// If you need any Routing, put it here.

ReactDOM.render(<App />, document.getElementById('react-app'));
registerServiceWorker();
