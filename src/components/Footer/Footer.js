import React from 'react';
import PropTypes from 'prop-types';

const componentName = props => {
    return (
        <footer>
        <img class="footer__logo" src="http://placehold.it/150" />
            <p>Is the Grass Greener? comes courtesy of Hillarys, the interiors expert inspiring
            the nation with beautiful blinds, curtains, shutters and carpets.</p>
        </footer>
    );
};

componentName.propTypes = {};

export default componentName;