import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Footer from '../Footer/Footer';
import InputContainer from './Input/InputContainer';

class Splash extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentWillMount() {}

    componentDidMount() {}

    componentWillReceiveProps(nextProps) {}

    shouldComponentUpdate(nextProps, nextState) {}

    componentWillUpdate(nextProps, nextState) {}

    componentDidUpdate(prevProps, prevState) {}

    componentWillUnmount() {}

    render() {
        return (
            <div>
                <h1>Is the Grass Greener?</h1>
                <p>Looking to move home? Or just fancy a nosy in another area? Take a look at
                    this nifty tool and compare where you live now with another location. Compare
                    everything from transport to schools and decide whether the grass really is
                    greener on the other side of the fence.</p>
                <InputContainer />
                <Footer />
            </div>
        );
    }
}

Splash.propTypes = {};

export default Splash;