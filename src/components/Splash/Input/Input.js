import React, {Component} from 'react';
import PropTypes from 'prop-types';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'

class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            address: 'San Francisco, CA'
        }
        this.onChange = (address) => this.setState({address})
    }

    handleFormSubmit = (event) => {
        event.preventDefault()

        geocodeByAddress(this.state.address)
            .then(results => getLatLng(results[0]))
            .then(latLng => console.log('Success', latLng))
            .catch(error => console.error('Error', error))
    }

    render() {
        const inputProps = {
            value: this.state.address,
            onChange: this.onChange
        }
        return (
            <form onSubmit={this.handleFormSubmit}>
                <h3>{this.props.title}</h3>
                <PlacesAutocomplete inputProps={inputProps}/>
                <button type="submit">Submit</button>
            </form>
        )
    }
}

Input.propTypes = {};

export default Input;