import React from 'react';
import PropTypes from 'prop-types';
import Input from './Input'

const InputContainer = props => {
    return (
        <div>
            <Input title="Where you live:"/>
            <Input title="Where you'd like to compare:"/>
        </div>
    );
};

InputContainer.propTypes = {
    
};

export default InputContainer;