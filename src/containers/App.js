import React, { Component } from 'react';
import Splash from '../components/Splash/Splash';

class App extends Component {
  constructor(){
    super();
    this.state = {
      view: <Splash changeView={this.changeView.bind(this)} />
    }
  }
  changeView(){
    this.setState({
      // view: <MainView /> // Changes state to main view component
    })
  }
  render() {

    return (
      <main>
        {this.state.view}
      </main>
    );
  }
}

export default App;